-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-09-2022 a las 01:11:41
-- Versión del servidor: 10.1.39-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `diario_digital`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `news`
--

CREATE TABLE `news` (
  `id` int(11) NOT NULL,
  `title` varchar(250) NOT NULL,
  `text` text NOT NULL,
  `img` varchar(350) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `news`
--

INSERT INTO `news` (`id`, `title`, `text`, `img`) VALUES
(1, 'La NASA denunció que China planea poner un supermercado en la luna.', 'Según la agencia espacial norteamericana, en las últimas semanas se interceptaron 3 cohetes chinos con mercadería y un cuarto, tripulado por personas de origen peruano entrenados para hacer reabastesimiento de mercadería en gravedad 0.', 'https://www.nasa.gov/sites/default/files/styles/full_width_feature/public/thumbnails/image/crs7-postlaunch1.jpg'),
(2, 'Crean software para detectar gente pelotudeando en el trabajo.', 'El Software, promete disparar la productividad de empresas y comercios, al eliminar tiempos muertos de empleados asignandoles diferentes roles. Varios sindicatos ya se declararon en contra del uso de esta tecnología.', 'https://1.bp.blogspot.com/-f1w3f4ePS3s/W1jVAAlT6RI/AAAAAAAAGuE/jAD5isAy2Ws5MNlrbViteOkzs3TTAr5nACLcBGAs/s640/SW_DRONE.jpg'),
(3, 'Quien es Tito Musk, el primo argentino de Elon Musk que se presentó en lo de Guido Kaczka.', 'Tito Musk vive en Bernal y desde chico se destacó por inventar pelotudeces. En 1986 intentó mandar un conejo a la Luna pero revento como el Challenger. También dice que habla seguido con su primo, Elon.', 'https://files.merca20.com/uploads/2022/05/Elon-Musk-tatuaje-e1651875137289.jpeg'),
(4, 'Starbucks se va del país y hay preocupación entre hipsters: -\'no sabemos donde iremos a mostrar que tenemos una macbook.\'-', 'Según el comunicado oficial \'debido a la crisis global que vive el sector, y la cantidad de pelotudos que ocupan mesas durante 8 horas con un café y dos mediaslunas solamente para darse corte que tienen una MacBook, se hace inviables la continidad de las operaciones en Argentina.', 'https://1.bp.blogspot.com/-HiD_vBlSbg0/X2PjR8udR5I/AAAAAAAAIvw/E3EWAortz7A3ZHIdTrOGHa7YGsf17ZUiQCLcBGAsYHQ/w640-h444/starbucks.jpg');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
