<?php
define('BASE_URL', '//' . $_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']) . '/');

require_once 'noticias.php';
require_once 'about.php';

//recibir/leer la accion
if (!empty($_GET['action'])) {
    $accion = $_GET['action'];
} else {
    $accion = 'noticias';
}

// parseo el string de action por "/" y me devuelve el arreglo
$params = explode('/', $accion);

//
switch ($params[0]) {
    case 'noticias':
        showNoticias();
        break;
    case 'noticia':
        showNoticia($params[1]);
        break;
    case 'about':
        if (empty($params[1])) {
            showAbout();
        } else {
            showAbout($params[1]);
        }
        break;
    default:
        echo ('404 page not found');
        break;
}
