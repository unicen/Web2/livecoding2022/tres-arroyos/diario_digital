<?php
function showNoticias(){
    require_once 'fakeDB.php';
    require_once 'templates/header.php';
    $noticiasDB = getNoticias(); 
      ?>
    <main class="container mt-5">
        <section class="noticias">
            <?php foreach($noticiasDB as $clave => $noticia){?>
            <div class="card">
            <img src="<?php echo $noticia->img?>" class="card-img-top" alt="...">
            <div class="card-body">
                <h5 class="card-title"><?php echo $noticia->title?></h5>
                <p class="card-text"><?php echo $noticia->text?></p>
                <a class="btn btn-outline-primary" href="<?php echo 'noticia/'.$clave;?>">Leer más</a>
            </div>
            </div>
            <?php } ?>       
        </section> 
    </main>
    <?php
    require_once 'templates/footer.html';
}

function showNoticia ($id){
    require_once 'fakeDB.php';
    require_once 'templates/header.php'; 
    if (!isset($id)){
        echo '<div class="alert alert-danger">No se especificó la noticia.</div>';
    }
    else if ($id<0 || $id>=count($noticiasDB)){
        echo '<div class="alert alert-danger">No existe noticia con ese id.</div>';
    }
    else{
        $noticia = $noticiasDB[$id]; 
    ?> 

    <section class="noticias">
        <div class="card">
            <img src="<?php echo $noticia->img?>" class="card-img-top" alt="...">
            <div class="card-body">
            <h5 class="card-title"><?php echo $noticia->title?></h5>
            <p class="card-text"><?php echo $noticia->text?></p>    
            </div>
        </div>
    </section>
    <?php }
    require_once 'templates/footer.html';
}?>