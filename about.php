<?php 
require_once 'fakeDB.php';

function showAbout($dev = null) {
  $developers = getDevelopers();
  include 'templates/header.php'; ?>

  <main class="container mt-5">
    <div class="row">
      <div class="col">
        <div class="list-group">
          <?php foreach ($developers as $key => $developer) { ?>
            <a class="list-group-item list-group-item-action" href="about/<?php echo $developer->id?>">
              <?php echo $developer->name?>
            </a>
          <?php }?>
        </div>
      </div>
        <?php } ?>
    </div>
        
      </div>
    </div>
  </main>

  <?php require 'templates/footer.html';